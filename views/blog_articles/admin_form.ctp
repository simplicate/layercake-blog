<h1>Edit Blog Article</h1>

    <?= $form->create( 'BlogArticle', array( 'id' => 'edit-form', 'type' => 'file' ) );?>

    <?= $form->input( 'id' ); ?>
    <?= $form->input( 'name', array( 'class' => 'required' ) ); ?>

    <div class="form-left">
        <?= $form->input('description', array( 'label' => "Summary Description", 'class' => 'required ck-simple', 'type' => 'textarea' ) ); ?>
    </div>

    <div class="form-right">
        <?= $form->input( 'status', array( 'type' => 'select', 'options' => array('draft'=>'Draft', 'published'=>'Published') ) ); ?>
        <div class="form-left">
            <?= $form->input( 'published_date', array( 'type' => 'text', 'class' => ' datepicker' ) ); ?>
            <script type="text/javascript">
                $(function() { $(".datepicker").datepicker( { dateFormat: 'yy-mm-dd' } ); });
            </script>
        </div>
        <div class="form-right">
            <?= $form->input( 'published_time', array( 'timeFormat' => 24 ) ); ?>
        </div>
        <div class="clear"></div>

        <? $user = $session->read('AdminAuth') ?>
        <?= $form->input( 'author',    array( 'default' => $user['admin_name'] ) ); ?>
        <?= $form->input( 'category',  array( 'type' => 'text' ) ); ?>
        <?= $form->input( 'tags' ); ?>
    </div>
    <div class="clear"></div>

    <?= $form->input( 'body', array( 'label' => "Blog Article Contents", 'class' => 'required ck-full', 'type' => 'textarea' ) ); ?>

    <div class="input">
        <?
            $image_url = "";
            if( !empty( $this->data['BlogArticle']['image_url'] ) ) {
                $image_url = '<img src="' . $this->data['BlogArticle']['image_url'] . '" width="462" />';
            }
        ?>
        <?= $form->input( 'image_url', array( 'label' => 'Associated Image', 'class' => 'required', 'style' => 'width:70%;', 'between' => $image_url, 'after' => '&nbsp;<input type="button" value="Browse Server" onclick="BrowseServer( \'Images:/\', \'BlogArticleImageUrl\' );" />' ) ); ?>

        <?= $form->input( 'display_image_on_page' ); ?>
    </div>
</div>

<br /><br />
<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link', array( "plugin" => "admin", "HistoryModel" => "Blog" ) ); ?>

<?= $this->element( 'auto_complete', array( "plugin" => "LayerCake", "field_id" => "BlogArticleTags",     "dictionary" => isset( $blog_tags  ) ? $blog_tags  : array() ) ); ?>
<?= $this->element( 'auto_complete', array( "plugin" => "LayerCake", "field_id" => "BlogArticleCategory", "dictionary" => isset( $categories ) ? $categories : array(), "multiple" => false ) ); ?>
<?= $this->element( 'ck_editor',     array( "plugin" => "LayerCake", "type" => array( "regular", "simple" ) ) ); ?>

<script type="text/javascript">
    function BrowseServer( startupPath, functionData )
    {
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();

        // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.basePath = '/layer_cake/ckfinder/';

        //Startup path in a form: "Type:/path/to/directory/"
        finder.startupPath = startupPath;

        // Name of a function which is called when a file is selected in CKFinder.
        finder.selectActionFunction = SetFileField;

        // Additional data to be passed to the selectActionFunction in a second argument.
        // We'll use this feature to pass the Id of a field that will be updated.
        finder.selectActionData = functionData;

        // Launch CKFinder
        finder.popup();
    }

    // This is a sample function which is called when a file is selected in CKFinder.
    function SetFileField( fileUrl, data )
    {
        document.getElementById( data["selectActionData"] ).value = fileUrl;
    }
</script>