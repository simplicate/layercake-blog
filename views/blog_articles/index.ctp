<?php
    $this->set( 'title_for_layout', Configure::read('Blog.title') );
    $this->params['url']['url'] = Configure::read('Blog.url');
    $this->set( "body_id", "blog" );
?>

<h1><?= Configure::read('Blog.title'); ?></h1>

<? if( count( $articles ) ): ?>
    <? $search_term = isset( $search_highlight ) ? $search_highlight : ''; ?>
    <? foreach ( $articles as $article ): ?>
        <? if( !empty( $article['BlogArticle']['image_url'] ) ): ?>
            <div><?= $this->Html->image( $article['BlogArticle']['image_url'] ); ?></div>
        <? endif; ?>

        <?php echo $this->Blog->header_link( $article, 'h3' ); ?>
        <?php echo $this->Blog->summary( $article, $search_term );   ?>
    <? endforeach; ?>

    <? if( $paginator->hasPage(2) ): ?>
        <p class="pagination">
            <?= $paginator->prev('«'); ?>
            <?= $paginator->numbers(); ?>
            <?= $paginator->next('»'); ?>
        </p>
    <? endif; ?>
<? else: ?>
    <p>No blog articles found.</p>
<? endif; ?>