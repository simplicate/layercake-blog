<?
    // blog index view
    $blog_url = Configure::read('Blog.url');
    Router::connect( $blog_url, array( 'plugin' => 'blog', 'controller' => 'blog_articles', 'action' => 'index' ) );

    // blog article view route
    Router::connect(
        "$blog_url/:year/:month/:slug/*",
        array(
                'plugin'     => 'blog',
                'controller' => 'blog_articles',
                'action'     => 'view',
        ),
        array(
            'pass'  => array( 'year', 'month', 'slug' ),
            'year'  => '[12][0-9]{3}',
            'month' => '0[1-9]|1[012]',
            'slug'  => '.+',
        )
    );

    // blog archive list route
    Router::connect(
        "$blog_url/:year/:month/*",
        array(
                'plugin'     => 'blog',
                'controller' => 'blog_articles',
                'action'     => 'archive',
                'month'      => null,
        ),
        array(
            'pass'  => array( 'year', 'month' ),
            'year'  => '[12][0-9]{3}',
            'month' => '0[1-9]|1[012]',
        )
    );

    // all other blog routes
    Router::connect( "$blog_url/:action/*", array( 'plugin' => 'blog', 'controller' => 'blog_articles' ) );
?>