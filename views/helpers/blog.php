<?php
class BlogHelper extends AppHelper {
    var $helpers = array( 'Html', 'Text', 'Time' );

    function link( $article ) {
        $post_time = strtotime( $article['BlogArticle']['created'] );
        $blog_url  = Configure::read('Blog.url') . "/" . date('Y', $post_time) . "/" . date('m', $post_time) . "/" . $article['BlogArticle']['slug'];
        return $blog_url;
    }

    function header_link( $article, $wrapper = 'h1' ) {
        return $this->Html->tag( $wrapper, $this->Html->link( $article['BlogArticle']['name'], $this->link( $article ) ), array( 'class' => 'post' ) );
    }

    function date( $article, $wrapper = null ) {
        $date = date( 'F jS, Y', strtotime($article['BlogArticle']['published_date']) );
        return ( isset( $wrapper ) ) ? $this->Html->tag( $wrapper, $date ) : $date;
    }

    function summary( $article, $highlight = null ) {
        $summary = !empty( $article['BlogArticle']['description'] ) ? strip_tags( $article['BlogArticle']['description'], "<a><b><i><strong><em><br>" ) : trim( substr( strip_tags( $article['BlogArticle']['body'] ), 0, 300 ) ) . "... ";
        $summary = $this->Text->highlight( $summary, $highlight );

        echo preg_replace( "/<\/p>/", " ", $summary );
        echo $this->Html->link( "Read More &rsaquo;", $this->link( $article ), array( 'escape' => false, 'class' => 'blog-read-more' ) );
    }

    function search_header( $search_term = null, $wrapper = 'h2' ) {
        if( empty( $search_term ) ) { return; }
        return $this->Html->tag( 'h4', "<em>" . sprintf(__( "Search Results for: %s", true), $search_term ) . "</em>" );
    }

    function tag_header( $tag_name = null, $wrapper = 'h2' ) {
        if( empty( $tag_name ) ) { return; }
        return $this->Html->tag( 'h4', "Topic: <em>$tag_name</em>" );
    }

    function category_header( $category = null, $wrapper = 'h2' ) {
        if( empty( $category ) ) { return; }
        return $this->Html->tag( 'h4', "Category: <em>$category</em>" );
    }

    function tags( $tags = array(), $wrapper = null, $before = null ) {
        if( !count( $tags ) ) { return; }

        // create a new list to hold the html
        $list = array();
        foreach( $tags as $tag => $count ) {
            $name = is_numeric( $tag ) ? $tags[$tag]['tag'] : $tag;
            $list[] = '<li>' . $this->Html->link( __( $name, true ), Configure::read('Blog.url') . "/tag/" . $name ) . '</li>';
        }

        // sort the tags
        asort( $list );

        // create the list of tags seperated by a comma
        $content  = isset( $before ) ? $before : "";
        $content .= implode( ", ", $list );

        // return the tags
        return ( isset( $wrapper ) ) ? $this->Html->tag( $wrapper, $content ) : $content;
    }

    function rss_time( $article ) {
        return trim( $this->Time->toRSS( strtotime( $article['BlogArticle']['created'] ) ) );
    }
}