<?php
    $this->set( 'title_for_layout', $author['Admin']['name'] );
    $this->params['url']['url'] = Configure::read('Blog.url');
    $this->set( "body_id", "blog" );
?>

<h1><?= $author['Admin']['name']; ?></h1>

<?= $author['Admin']['bio']; ?>