<h1>Blog Articles</h1>

<? if( sizeof( $blogArticles ) == 0 ): ?>
    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>

    <?= $html->link( "New Blog Post", '/admin/blog/add', array( 'class' => 'button', 'escape' => false )  ); ?>
<? else: ?>

    <?= $paginator->options( array( 'url' => $this->passedArgs ) ); ?>

    <?= $html->link( "New Blog Post", '/admin/blog/add', array( 'class' => 'button', 'escape' => false )  ); ?>
    <table class="list">
        <thead>
            <tr>
                <th><?= $paginator->sort('name');?></th>
                <th><?= $paginator->sort('category');?></th>
                <th><?= $paginator->sort('status');?></th>
                <th><?= $paginator->sort('Published', 'published_date');?></th>
                <th class="t-center">Actions</th>
            </tr>
        </thead>

        <tbody>
            <? foreach( $blogArticles as $blogArticle ): ?>
                <tr class="<?= $cycle->cycle('', 'altrow'); ?>">
                    <td>
                        <b><?= $blogArticle['BlogArticle']['name']; ?></b><br />
                        <?= strip_tags( $blogArticle['BlogArticle']['description'] ); ?>
                        <? if( !empty( $blogArticle['BlogArticle']['tags'] ) ): ?>
                            <br />
                            <span class="small green">Tags: <i><?= $blogArticle['BlogArticle']['tags']; ?></i></span>
                        <? endif; ?>
                    </td>
                    <td><?php echo $blogArticle['BlogArticle']['category']; ?></td>
                    <td><?php echo $blogArticle['BlogArticle']['status']; ?></td>
                    <td><?php echo $blogArticle['BlogArticle']['published_date']; ?></td>
                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png"   alt="Edit"   />', array( 'action' => 'edit',   $blogArticle['BlogArticle']['id']), array( 'escape' => false ) ); ?>
                        |
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-delete.png" alt="Delete" />', array( 'action' => 'delete', $blogArticle['BlogArticle']['id']), array( 'escape' => false ), sprintf(__('Are you sure you want to delete %s?', true), $blogArticle['BlogArticle']['name']) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>

    <!-- Pagination -->
    <div class="pagination bottom box">
        <p class="f-left"><?= $paginator->counter(); ?></p>

        <p class="f-right">
            <?= $paginator->prev('«'); ?>
            <?= $paginator->numbers();		 ?>
            <?= $paginator->next('»'); ?>
        </p>
    </div>
<? endif; ?>