<?php
class BlogArticle extends AppModel {

	var $name   = 'BlogArticle';
    var $hasAndBelongsToMany = array( 'Tag' => array( 'className' => 'LayerCake.Tag' ) );
    var $actsAs = array(
        'Utils.Sluggable',
        'LayerCake.Searchable' => array( 'ignoreColumns' => array( 'status' ), 'summaryColumns' => array( 'description', 'body' ) ),
        'LayerCake.Taggable',
    );

    var $belongsTo = array( 'Admin' => array( 'className' => 'LayerCake.Admin', 'foreignKey' => 'admin_id' ) );


    function beforeSave( ) {

        // only do this if we're saving / updating the status
        if( $this->data['BlogArticle']['status'] ) {
            // if the article is "published"
            if( $this->data['BlogArticle']['status'] == 'published' ) {

                // if there is no published time
                if( empty( $this->data['BlogArticle']['published_time'] ) || $this->data['BlogArticle']['published_time'] == '00:00:00' ) {
                    $this->data['BlogArticle']['published_time'] = date( 'H:i:s' );
                }

                // if there is no published date
                if( empty( $this->data['BlogArticle']['published_date'] ) || $this->data['BlogArticle']['published_date'] == '0000-00-00' ) {
                    $this->data['BlogArticle']['published_date'] = date( 'Y-m-d' );
                }

            // if it's in draft, delete the published date information
            } else {
                $this->data['BlogArticle']['published_date'] = '0000-00-00';
                $this->data['BlogArticle']['published_time'] = '00:00:00';
            }
        }

        return 1;
    }


    function loadForDisplay( $slug )
    {
        $article = $this->findBySlug( $slug );

        // increment view counter
        $this->updateAll(
            array( 'BlogArticle.views'  => 'BlogArticle.views + 1' ),
            array( 'BlogArticle.id'     => $article['BlogArticle']['id'] )
        );

        return $article;
    }


    function listForSitemap()
    {
        $articles = $this->findAllByStatus( "published" );
        $sitemap  = array();

        foreach ( $articles as $article ) {
            $post_time = strtotime( $article['BlogArticle']['created'] );
            $post_link = Configure::read('Blog.url') . '/' . date( 'Y', $post_time ) . '/' . date( 'm', $post_time ) . '/' . $article['BlogArticle']['slug'];

            $sitemap[] = array(
                'title'    => $article['BlogArticle']['name'],
                'url'      => $post_link,
                'modified' => $article['BlogArticle']['modified'],
            );
        }

        return $sitemap;
    }

    function searchable_link( $data ) {
        $url = date( 'Y/m', strtotime( $data['BlogArticle']['published_date'] ) ) . '/' . $data['BlogArticle']['slug'];
        return Configure::read('Blog.url') . '/' . $url;
    }
}
?>
