<?php
    // import required model
    App::Import( 'Model', 'Blog.BlogArticle' );
    $BlogArticle  = new BlogArticle();
    
    // make sure we're only finding published blog
    $find_conditions = array(
        "BlogArticle.status" => "published"
    );
    
    // do we want blogs for a specific tag
    if( isset( $tag ) ) {
        $find_conditions["BlogArticle.tags LIKE"] = "%" . $tag . "%";
    }
    
    // how many blogs do we want
    $counter = isset( $limit ) ? $limit : 1;
    
    // find the cached version of this blog query
    $cache_key  = md5( serialize( array( $find_conditions, $counter ) ) );
    $blog_cache = Cache::read( 'blog_articles' , 'default' );
    $blog_cache = ( is_array( $blog_cache ) ) ? $blog_cache : array();
    $articles   = ( isset( $blog_cache[$cache_key] ) ) ? $blog_cache[$cache_key] : array();
    
    // find all articles
    if( !count( $articles ) ) {
        $articles = $BlogArticle->find( 'all', array( 'conditions' => $find_conditions, 'order' => array( 'published_date DESC', 'published_time DESC' ), 'limit' => $counter ) );
    
        // do a little touching up of all the articles
        foreach( $articles AS &$article ) {
            $post_time = strtotime( $article['BlogArticle']['published_date'] );
            $article['BlogArticle']['slug_url'] = date('Y', $post_time) . "/" . date('m', $post_time) . "/" . $article['BlogArticle']['slug'];        
        }
        
        // write to cache
        $blog_cache[$cache_key] = $articles;
        Cache::write( 'blog_articles', $blog_cache, 'default' );
    }
    
    // return inside a layout
    echo $this->element( $layout, array( "articles" => $articles ) );
?>