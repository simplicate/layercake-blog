<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class BlogArticlesController extends LayerCakeAppController {

    var $helpers = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'Xml', 'Blog.Blog' );
    var $components = array( 'Auth', 'Session' );
    var $uses = array(
                    'Blog.BlogArticle',
                    'LayerCake.SearchIndex',
                    'Blog.BlogArticlesTags',
                    'LayerCake.Admin',
                );

	function beforeFilter() {
		parent::beforeFilter( );
		$this->Auth->allow( 'index', 'archive', 'view', 'tag', 'rss', 'author', 'category' );
	}


    // show all blog articles
    function index() {

        if( Configure::read('debug') == 0 ) {
            $this->helpers[]   = 'Cache';
            $this->cacheAction = '10 minutes';
        }

        $this->paginate = array(
            'limit' => 4,
            'order' => array( 'BlogArticle.published_date' => 'desc', 'BlogArticle.published_time' => 'desc' ),
            'conditions' => array( "BlogArticle.status" => "published" ),
        );

        $this->set( 'articles', $this->paginate('BlogArticle') );
    }


    // show a single blog article
    function view( $year = '', $month = '', $slug = '' ) {

        if( Configure::read('debug') == 0 ) {
            $this->helpers[]   = 'Cache';
            $this->cacheAction = '10 minutes';
        }

        // load the article
        $article = $this->BlogArticle->loadForDisplay( $slug );

        // make sure we have something
        if( empty( $article ) ) {
            $this->redirect( Configure::read( 'Blog.url' ) );
            exit;
        }

        $this->set( 'article', $article );
    }


    // show all entries for a particular month or year
    function archive( $year = null, $month = null ) {

        if( Configure::read('debug') == 0 ) {
            $this->helpers[]   = 'Cache';
            $this->cacheAction = '10 minutes';
        }

        // we need a year
        if( empty( $year ) || strlen( $year ) != 4 ) {
            $year = preg_replace( "/[^\d]/", "", $year);
            $this->redirect( Configure::read( 'Blog.url' ) );
        }

        // the month needs to be a month
        if( !empty( $month ) ) {
            $month = preg_replace( "/[^\d]/", "", $month );
            if( strlen( $month ) != 2 ) {
                $this->redirect( Configure::read( 'Blog.url' ) . '/archive/' . $year );
            }
        }

        // find start / end dates of the month
        $start_month= $month ? $month : 1;
        $end_month  = $month ? $month : 12;
        $max_days   = cal_days_in_month( CAL_GREGORIAN, $end_month, $year );

        $this->paginate['conditions'] = array(
            "BlogArticle.created >" => date('Y-m-d', strtotime("$start_month/01/$year") ),
            "BlogArticle.created <" => date('Y-m-d', strtotime("$end_month/$max_days/$year") ),
            "BlogArticle.status"    => "published",
        );

        $this->set( 'articles', $this->paginate('BlogArticle') );
        $this->render( 'index' );
    }


    // show author bio
    function author() {

        if( Configure::read('debug') == 0 ) {
            $this->helpers[]   = 'Cache';
            $this->cacheAction = '10 minutes';
        }

        $author_slug = isset( $this->params['pass'][0] ) ? $this->params['pass'][0] : '';
        $author = $this->Admin->findBySlug( $author_slug );

        // make sure we have something
        if( empty( $author ) ) {
            $this->redirect( Configure::read( 'Blog.url' ) );
            exit;
        }

        $this->set( 'author', $author );
        $this->render( 'author' );
    }


    // show all articles of a particular category
    function category() {

        if( Configure::read('debug') == 0 ) {
            $this->helpers[]   = 'Cache';
            $this->cacheAction = '10 minutes';
        }

        $this->paginate['conditions'] = array(
            "category"           => $this->passedArgs[0],
            "BlogArticle.status" => "published",
        );

        $this->paginate["order"] = array( 'BlogArticle.published_date' => 'desc', 'BlogArticle.published_time' => 'desc' );

        $this->set( 'articles', $this->paginate('BlogArticle') );
        $this->set( 'category', $this->passedArgs[0] );
        $this->render( 'index' );
    }


    // show all articles of a particular tag
    function tag() {

        if( Configure::read('debug') == 0 ) {
            $this->helpers[]   = 'Cache';
            $this->cacheAction = '10 minutes';
        }

        $tag_name = isset($this->passedArgs[0] ) ? $this->passedArgs[0] : '';
        $this->paginate['conditions'] = array(
            "tags LIKE"          => "%" . $tag_name . "%",
            "BlogArticle.status" => "published",
        );

        $this->paginate["order"] = array( 'BlogArticle.published_date' => 'desc', 'BlogArticle.published_time' => 'desc' );

        $this->set( 'articles', $this->paginate('BlogArticle') );
        $this->set( 'tag_name', $tag_name );
        $this->render( 'index' );
    }


    // rss feed
    function rss() {
        //$this->helpers[]   = 'Cache';
        //$this->cacheAction = '10 minutes';

        if( Configure::read('debug') ) { Configure::write( 'debug', 2 ); }
        $this->helpers[] = 'Rss';

        $find = array(
            'conditions' => array( "BlogArticle.status" => "published" ),
            'limit'      => 15,
            'order'      => array( "BlogArticle.published_date DESC", "BlogArticle.published_time DESC" )
        );

        $articles = $this->BlogArticle->find( 'all', $find );

        $this->set( 'articles', $articles );
        $this->layout = 'rss';
    }


	function admin_index() {
        $this->disableCache();

        $this->paginate = array(
            'order' => array( 'BlogArticle.status' => 'asc',  'BlogArticle.published_date DESC', 'BlogArticle.published_time DESC' ),
        );

		$this->BlogArticle->recursive = 1;

        if( !empty( $this->params['form']['q'] ) ) {
			$this->redirect( "/admin/blog/index/q:" . $this->params['form']['q'] );
		}

		if( !empty( $this->params['named']['q'] ) ) {
			$this->paginate['conditions'] = array(
				"OR" => array (
					"BlogArticle.name LIKE" 	   => "%" . $this->params['named']['q'] . "%",
					"BlogArticle.description LIKE" => "%" . $this->params['named']['q'] . "%",
                    "BlogArticle.body LIKE"        => "%" . $this->params['named']['q'] . "%",
                    "BlogArticle.tags LIKE"        => "%" . $this->params['named']['q'] . "%",
                    "BlogArticle.status LIKE"      => "%" . $this->params['named']['q'] . "%",
				)
			);
		}

		$this->set( 'blogArticles', $this->paginate( 'BlogArticle' ) );
	}


	function admin_add() {
        $this->disableCache();
		if( strstr( $this->referer(), '/blog_articles/index' ) || $this->referer() == '/admin/blog_articles/' || $this->referer() == '/admin/blog_articles' ) {
			$this->Session->write( "History.Blog.Add", $this->referer() );
		}

		if (!empty($this->data)) {

			$this->BlogArticle->create();

            App::import( 'Vendor', 'LayerCake.htmlcleaner' );
            $cleaner = new htmlcleaner;
            $this->data['BlogArticle']['body'] = $cleaner->clean_img_floats( $this->data['BlogArticle']['body'] );
            $this->data['BlogArticle']['body'] = $cleaner->clean_inline_styles( $this->data['BlogArticle']['body'] );

            // try to save the record
            if ($this->BlogArticle->save($this->data)) {
                $this->Session->setFlash(__('The Blog Article has been saved', true), 'default', array('class' => 'success') );
                $history  = $this->Session->read( "History.Blog.Add" );
                $this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );

            // bad save
            } else {
                $this->Session->setFlash(__('The Blog Article could not be saved. Please, try again', true), 'default', array('class' => 'error') );
            }
		}

        // get categories
        $categories = $this->BlogArticle->query( "SELECT DISTINCT(`category`) FROM `blog_articles`" );
        $categories = Set::extract($categories, '{n}.blog_articles.category');
        $categories = empty( $categories[0] ) ? array() : $categories;

        $this->set( 'blog_tags', $this->BlogArticle->Tag->find('list', array( 'fields' => array('Tag.tag'), 'recursive' => 0 ) ) );
        $this->render( 'admin_form' );
	}


	function admin_edit($id = null) {
        $this->disableCache();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Blog Article', true), 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}

		if( strstr( $this->referer(), '/blog_articles/index' ) || $this->referer() == '/admin/blog_articles/' || $this->referer() == '/admin/blog_articles' ) {
			$this->Session->write( "History.Blog.Edit." . $id, $this->referer() );
		}

		if (!empty($this->data)) {

            App::import( 'Vendor', 'LayerCake.htmlcleaner' );
            $cleaner = new htmlcleaner;
            $this->data['BlogArticle']['body'] = $cleaner->clean_img_floats( $this->data['BlogArticle']['body'] );
            $this->data['BlogArticle']['body'] = $cleaner->clean_inline_styles( $this->data['BlogArticle']['body'] );

            // try to save the record
            if ($this->BlogArticle->save($this->data)) {
                $this->Session->setFlash(__('The Blog Article has been saved', true), 'default', array('class' => 'success') );
                $history  = $this->Session->read( "History.Blog.Edit." . $id );
                $this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );

            // bad save
            } else {
                $this->Session->setFlash(__('The Blog Article could not be saved. Please, try again', true), 'default', array('class' => 'error') );
            }
		}

		if (empty($this->data)) {
			$this->data = $this->BlogArticle->read(null, $id);
		}

        // get categories
        $categories = $this->BlogArticle->query( "SELECT DISTINCT(`category`) FROM `blog_articles`" );
        $categories = Set::extract($categories, '{n}.blog_articles.category');
        $categories = empty( $categories[0] ) ? array() : $categories;

        $this->set( 'blog_tags', $this->BlogArticle->Tag->find('list', array( 'fields' => array('Tag.tag'), 'recursive' => 0 ) ) );
        $this->set( 'categories', $categories );
        $this->render( 'admin_form' );
	}


	function admin_delete($id = null) {
        $this->disableCache();
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Blog Article', true), 'default', array('class' => 'error') );
			$this->redirect( $this->referer() );
		}
		if ($this->BlogArticle->delete($id)) {
			$this->Session->setFlash(__('Blog Article deleted', true), 'default', array('class' => 'success') );
			$this->redirect( $this->referer() );
		}
    }
}