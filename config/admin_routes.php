<?
    Router::connect( '/admin/blog',                     array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'blog', 'controller' => 'blog_articles', 'action' => 'index' ) );
    Router::connect( '/admin/blog/:action/*',           array( 'admin' => true, 'prefix' => 'admin', 'plugin' => 'blog', 'controller' => 'blog_articles' ) );
?>