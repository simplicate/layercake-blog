<?
    // import required model
    App::Import( 'Model', 'Blog.BlogArticle' );
    $BlogArticle  = new BlogArticle();

    $categories = $BlogArticle->query( "SELECT DISTINCT(`category`) FROM `blog_articles`" );
    $categories = Set::extract($categories, '{n}.blog_articles.category');
    sort( $categories );
?>

<? foreach( $categories AS $category ): ?>
	<li><a href="<?= Configure::read('Blog.url'); ?>/category/<?= rawurlencode( $category ); ?>"><?= $category; ?></a></li>
<? endforeach; ?>