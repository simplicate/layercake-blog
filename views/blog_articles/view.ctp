<?php
    $this->set( 'title_for_layout', $article['BlogArticle']['name'] );
    $this->params['url']['url'] = Configure::read('Blog.url');
    $this->set( "body_id", "blog" );
?>

<?= $blog->header_link( $article, 'h1' ); ?>
<?= $blog->date( $article, 'h2' ); ?>

<?= $article['BlogArticle']['body']; ?>

<ul class="tags">
    <li>Tags</li>
    <?=$blog->tags( $article['Tag'] );?>
</ul>