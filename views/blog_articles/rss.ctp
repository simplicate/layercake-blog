<?php
    $this->set('documentData', array(
        'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

    $this->set('channelData', array(
        'title'       => __( "Simplicate Interactive", true ),
        'link'        => $html->url('/', true),
        'description' => __( "Simplicate Interactive is a Website Design, Development, and Interactive Marketing Strategy agency based out of Toronto, Ontario, Canada", true ),
        'language'    => __( 'en-us', true )));

    foreach ( $articles as $article ) {
        echo "<item>\n";
        echo "  <title>" . $article['BlogArticle']['name'] . "</title>\n";
        echo "  <link>" . $html->url('/', true) . $blog->link( $article ) . "</link>\n";
        echo "  <guid>" . $blog->link( $article ) . "</guid>\n";
        echo "  <description><![CDATA[ " . $article['BlogArticle']['body'] . " ]]></description>\n";
        echo "  <pubDate>" . $blog->rss_time( $article ) . "</pubDate>\n";
        echo "</item>\n\n";
    }
?>