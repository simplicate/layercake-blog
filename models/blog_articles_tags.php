<?php
class BlogArticlesTags extends AppModel {

	var $name   = 'BlogArticlesTags';
    
    function top_tags() {
        $tags  = $this->query("SELECT COUNT( blog_article_id ) AS 'count', tag AS 'name' FROM `blog_articles_tags` LEFT JOIN `tags` ON `blog_articles_tags`.tag_id = `tags`.id WHERE 1 GROUP BY tag_id");
        $usage = array();
        
        foreach($tags as $tag) {
            $usage = array_merge( $usage, array($tag['tags']['name'] => $tag[0]['count']) );
        }
        
        return $usage;
    }
    
}
?>